﻿using Microsoft.Xna.Framework.Content;
using System;

namespace Together.Schema
{
    public abstract class SceneData
    {
        [ContentSerializer]
        public string ID { get; set; }

        [ContentSerializer]
        public string NextSceneID { get; set; }
    }
}
