﻿
using Microsoft.Xna.Framework.Content;

namespace Together.Schema
{
    public class MatrixTransitionData : TransitionData
    {
        [ContentSerializer]
        public int MinSpeed { get; set; }

        [ContentSerializer]
        public int MaxSpeed { get; set; }

        [ContentSerializer]
        public bool IsReversed { get; set; }
    }
}
