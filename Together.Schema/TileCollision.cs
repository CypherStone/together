﻿namespace Together.Schema
{
    public enum TileCollision
    {
        None = 0,
        Left = 1,
        Right = 2,
        Top = 4,
        Bottom = 8
    }
}
