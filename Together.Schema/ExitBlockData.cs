﻿namespace Together.Schema
{
    using Microsoft.Xna.Framework.Content;

    public class ExitBlockData : BlockData
    {
        [ContentSerializer]
        public bool IsCenter { get; set; }
    }
}
