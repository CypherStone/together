﻿using Microsoft.Xna.Framework.Content;
using System;

namespace Together.Schema
{
    public class TransitionData
    {
        [ContentSerializer]
        public string Image { get; set; }

        [ContentSerializer]
        public LevelTransitions Transition { get; set; }
    }
}
