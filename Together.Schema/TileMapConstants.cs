﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Together.Schema
{
    public static class TileMapConstants
    {
        public const int MapWidth = 128;
        public const int MapHeight = 72;
    }
}
