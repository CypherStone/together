﻿namespace Together.Schema
{
    using Microsoft.Xna.Framework.Content;

    public class MenuSceneData : SceneData
    {
        [ContentSerializer]
        public string Title { get; set; }

        [ContentSerializer]
        public string StartPrefix { get; set; }

        [ContentSerializer]
        public string StartPostfix { get; set; }
    }
}
