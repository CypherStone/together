﻿using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Text;

namespace Together.Schema
{
    public class PlayerStartData : TileData
    {
        [ContentSerializer]
        public int Index { get; set; }
    }
}
