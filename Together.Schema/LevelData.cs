﻿namespace Together.Schema
{
    using Microsoft.Xna.Framework.Content;

    public class LevelData : SceneData
    {
        [ContentSerializer]
        public string TileMap { get; set; }

        [ContentSerializer (Optional = true)]
        public bool IsFinale { get; set; }
    }
}

