﻿using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Text;

namespace Together.Schema
{
    public class PickupData : TileData
    {
        [ContentSerializer]
        public string Image { get; set; }
    }
}
