﻿
namespace Together.Schema
{
    using Microsoft.Xna.Framework.Content;
    using System.Collections.Generic;

    public class ScenesData
    {
        [ContentSerializer]
        public List<SceneData> Scenes { get; set; } 
    }
}
