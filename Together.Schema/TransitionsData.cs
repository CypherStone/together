﻿using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;

namespace Together.Schema
{
    public class TransitionsData
    {
        [ContentSerializer]
        public List<TransitionData> Transitions { get; set; }
    }
}
