﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Together.Schema
{
    public static class PlayerConstants
    {
        public const float Scale = 8.0f;

        public const float MovementSpeed = 5.0f;
        public const float MaxMovementSpeed = 25.0f;

        public const float Gravity = 70.0f;
        public const float Friction = 100.0f;
        public const float JumpVelocity = 40.0f;
    }
}
