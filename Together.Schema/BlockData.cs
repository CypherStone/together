﻿namespace Together.Schema
{
    using Microsoft.Xna.Framework.Content;

    public class BlockData : TileData
    {
        [ContentSerializer]
        public string Image { get; set; }
    }
}
