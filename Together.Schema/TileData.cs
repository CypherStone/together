﻿namespace Together.Schema
{
    using Microsoft.Xna.Framework.Content;

    public class TileData
    {
        public TileData()
        {
            this.Scale = 1.0f;
        }

        [ContentSerializer]
        public bool IsCollidable { get; set; }

        [ContentSerializer]
        public bool NoTileScaling { get; set; }

        [ContentSerializer]
        public float Scale { get; set; }
    }
}
