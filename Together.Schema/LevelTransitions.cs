﻿
namespace Together.Schema
{
    public enum LevelTransitions
    {
        Intro,
        Outro,
        Death,
        DeathReset,

        FinaleOutro,
        FinaleOutroFade,

        Total
    }
}
