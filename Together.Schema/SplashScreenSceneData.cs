﻿namespace Together.Schema
{
    using Microsoft.Xna.Framework.Content;
    using System.Collections.Generic;

    public class SplashScreenSceneData : SceneData
    {
        [ContentSerializer]
        public List<string> Images { get; set; }

        [ContentSerializer]
        public string Music { get; set; }

        [ContentSerializer]
        public float Scale { get; set; }
    }
}
