﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Text;
using Together.Scenes;
using Together.Schema;
using Together.Services;
using Together.TransitionOverlay;

namespace Together.Levels
{
    public class LevelCommonDataService : ILevelCommonDataService
    {
        private readonly IGameViewportService _viewportService;

        public SpriteFont Font { get; private set; }
        public ITransitionOverlay[] TransitionOverlays { get; private set; }
        public LevelBackground Background { get; private set; }

        public Song BackgroundMusic { get; private set; }
        public Song FinaleMusic { get; private set; }

        public SoundEffect AcceptSound { get; private set; }
        public SoundEffect JumpSound { get; private set; }
        public SoundEffect BumpSound { get; private set; }
        public SoundEffect PickupSound { get; private set; }
        public SoundEffect PickupsCollectedSound { get; private set; }
        public SoundEffect DeathSound { get; private set; }
        public SoundEffect WarpSound { get; private set; }
        public SoundEffect LevelCompleteSound { get; private set; }
        public SoundEffect LevelFailedSound { get; private set; }

        public Dictionary<Color, TileData> TilesDict { get; private set; }

        public float PlayerMovementSpeed { get; private set; }
        public float PlayerMaxMovementSpeed { get; private set; }
        public float PlayerGravity { get; private set; }
        public float PlayerFriction { get; private set; }
        public float PlayerJumpVelocity { get; private set; }

        public LevelCommonDataService(IGameViewportService viewportService)
        {
            this._viewportService = viewportService;

            this.TransitionOverlays = new ITransitionOverlay[(int)LevelTransitions.Total];
            this.Background = new LevelBackground();

            this.TilesDict = new Dictionary<Color, TileData>();
            this.TilesDict.Add(Color.White, new BlockData() { Image = "tile", IsCollidable = true });
            this.TilesDict.Add(Color.Yellow, new PickupData() { Image = "pickup", IsCollidable = true, NoTileScaling = true, Scale = 2.0f });

            this.TilesDict.Add(new Color(255, 0, 0, 255), new DeathBlockData() { Image = "spike_down", IsCollidable = true });
            this.TilesDict.Add(new Color(200, 0, 0, 255), new DeathBlockData() { Image = "spike_up", IsCollidable = true });
            this.TilesDict.Add(new Color(155, 0, 0, 255), new DeathBlockData() { Image = "spike_left", IsCollidable = true });
            this.TilesDict.Add(new Color(100, 0, 0, 255), new DeathBlockData() { Image = "spike_right", IsCollidable = true });

            this.TilesDict.Add(new Color(0, 255, 0, 255), new PlayerStartData() { Index = 0, IsCollidable = false });
            this.TilesDict.Add(Color.Green, new PlayerStartData() { Index = 1, IsCollidable = false });
            this.TilesDict.Add(Color.Gray, new BarrierBlockData() { Image = "barrier", IsCollidable = true });

            this.TilesDict.Add(new Color(0, 155, 255, 255), new ExitBlockData() { Image = "exit", IsCollidable = true, IsCenter = true });
            this.TilesDict.Add(new Color(0, 128, 255, 255), new ExitBlockData() { Image = "exit", IsCollidable = false, IsCenter = true });
            this.TilesDict.Add(new Color(0, 10, 255, 255), new ExitBlockData() { Image = "exit_border", IsCollidable = false });
        }
        public void LoadContent(ContentManager contentManager)
        {
            this.Font = contentManager.Load<SpriteFont>("Font/Main");

            TransitionsData transitionsData = contentManager.Load<TransitionsData>("Transitions/Transitions");

            foreach (TransitionData transitionData in transitionsData.Transitions)
            {
                if (transitionData is MatrixTransitionData matrixTransitionData)
                {
                    ITransitionOverlay transition = new MatrixPixelTransitionOverlay(this._viewportService, matrixTransitionData);

                    this.TransitionOverlays[(int)matrixTransitionData.Transition] = transition;

                    transition.LoadContent(contentManager);
                }
            }

            this.Background.LoadContent(contentManager);

            this.BackgroundMusic = contentManager.Load<Song>("Music/background");
            this.FinaleMusic = contentManager.Load<Song>("Music/finale");

            this.AcceptSound = contentManager.Load<SoundEffect>("Sounds/accept");
            this.JumpSound = contentManager.Load<SoundEffect>("Sounds/jump");
            this.BumpSound = contentManager.Load<SoundEffect>("Sounds/bump");
            this.PickupSound = contentManager.Load<SoundEffect>("Sounds/pickup");
            this.PickupsCollectedSound = contentManager.Load<SoundEffect>("Sounds/pickups_collected");
            this.DeathSound = contentManager.Load<SoundEffect>("Sounds/death");
            this.WarpSound = contentManager.Load<SoundEffect>("Sounds/warp");
            this.LevelCompleteSound = contentManager.Load<SoundEffect>("Sounds/level_complete");
            this.LevelFailedSound = contentManager.Load<SoundEffect>("Sounds/level_failed");
        }

        public void Update(GameTime gameTime)
        {
            this.Background.Update(gameTime);
        }

        public void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch)
        {
            this.Background.Draw(graphicsDevice, spriteBatch);

            float tileScale = this._viewportService.TileScale;

            this.PlayerMovementSpeed     = PlayerConstants.MovementSpeed * tileScale;
            this.PlayerMaxMovementSpeed  = PlayerConstants.MaxMovementSpeed * tileScale;
            this.PlayerGravity           = PlayerConstants.Gravity * tileScale;
            this.PlayerFriction          = PlayerConstants.Friction * tileScale;
            this.PlayerJumpVelocity      = PlayerConstants.JumpVelocity * tileScale;
        }

        public void Reset()
        {
            this.Background.Reset();
        }
    }
}
