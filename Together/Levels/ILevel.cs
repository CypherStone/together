﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Together.Levels
{
    public interface ILevel
    {
        LevelState State { get; }
    }
}
