﻿
namespace Together.Levels
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Media;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Together.Game.Character;
    using Together.Game.Input;
    using Together.Levels.Tiles;
    using Together.Scenes;
    using Together.Schema;
    using Together.Services;
    using Together.TransitionOverlay;

    public class Level : IScene, ILevel
    {
        private readonly GameServiceContainer _serviceProvider;
        private TileMap _tileMap;

        public SceneData Data { get; private set; }
        public LevelData LevelData { get; private set; }
        public IScene NextScene { get; set; }
        public bool IsCompleted => this.State == LevelState.OutroCompleted;
        public LevelState State { get; private set; }

        public Level(LevelData data, GameServiceContainer serviceProvider)
        {
            this.Data = data;
            this.LevelData = data;
            this._serviceProvider = serviceProvider;

            this._tileMap = new TileMap(data, serviceProvider.GetService<IGameViewportService>(), serviceProvider.GetService<ILevelCommonDataService>());
        }
  
        public void LoadContent(ContentManager contentManager)
        {
            this._tileMap.LoadContent(contentManager);
        }

        public void Start()
        {
            this.SetNextState(LevelState.Intro);

            IPlayerManager playerManager = this._serviceProvider.GetService<IPlayerManager>();

            for (int i = 0; i < playerManager.Players.Length; ++i)
            {
                IPlayer player = playerManager.Players[i];

                player.SetupForLevel(this._tileMap.PlayerStarts[i]);
            }
        }

        public void Update(GameTime gameTime)
        {
            IPlayerManager playerManager = this._serviceProvider.GetService<IPlayerManager>();
            ILevelCommonDataService commonDataService = this._serviceProvider.GetService<ILevelCommonDataService>();

            if (this.State != LevelState.Play)
            {
                this._tileMap.Update(gameTime, playerManager, false);
            }

            switch (this.State)
            {
                case LevelState.Intro:
                    {
                        this.UpdateTransition(gameTime, commonDataService.TransitionOverlays, LevelTransitions.Intro, LevelState.Play);
                    }
                    break;
                case LevelState.Play:
                    {
                        commonDataService.Update(gameTime);

                        playerManager.Update(gameTime);

                        this._tileMap.Update(gameTime, playerManager, true);

                        playerManager.PostUpdate(gameTime);

                        if (playerManager.Players.Where(p => p.IsInLevel && p.IsDead).Any() == true)
                        {
                            this.SetNextState(LevelState.Death);
                        }
                        else if (playerManager.Players.All(p => (p.IsInLevel && p.HasExited) || !p.IsInLevel) == true)
                        {
                            this.SetNextState(LevelState.Outro);
                        }
                    }
                    break;
                case LevelState.Death:
                    {
                        this.UpdateTransition(gameTime, commonDataService.TransitionOverlays, LevelTransitions.Death, LevelState.DeathReset);
                    }
                    break;
                case LevelState.DeathReset:
                    {
                        this.UpdateTransition(gameTime, commonDataService.TransitionOverlays, LevelTransitions.DeathReset, LevelState.Play);
                    }
                    break;
                case LevelState.Outro:
                    {
                        int outroIndex = this.LevelData.IsFinale ? (int)LevelTransitions.FinaleOutro : (int)LevelTransitions.Outro;

                        this.UpdateTransition(gameTime, commonDataService.TransitionOverlays, (LevelTransitions)outroIndex, LevelState.OutroCompleted);
                    }
                    break;
                case LevelState.OutroCompleted:
                    break;
                default:
                    break;
            }
        }

        public void UpdateInput(GameTime gameTime)
        {
            if (this.State != LevelState.Play)
            {
                return;
            }

            IInputManager inputManager = this._serviceProvider.GetService<IInputManager>();
            IPlayerManager playerManager = this._serviceProvider.GetService<IPlayerManager>();

            foreach (IPlayer player in playerManager.Players)
            {
                if (player.IsInLevel == false)
                {
                    continue;
                }

                player.UpdateInput(inputManager, gameTime);
            }

#if DEBUG
            if (inputManager.GetGamePad(PlayerIndex.One).IsButtonPressed(Microsoft.Xna.Framework.Input.Buttons.B))
            {
                this.SetNextState(LevelState.Outro);
            }
#endif // DEBUG
        }

        public void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch)
        {
            graphicsDevice.Clear(Color.Black);

            ILevelCommonDataService commonDataService = this._serviceProvider.GetService<ILevelCommonDataService>();
            commonDataService.Draw(graphicsDevice, spriteBatch);

            this._tileMap.Draw(graphicsDevice, spriteBatch);

            IPlayerManager playerManager = this._serviceProvider.GetService<IPlayerManager>();
            playerManager.Draw(spriteBatch, graphicsDevice);

            switch (this.State)
            {
                case LevelState.Intro:
                    commonDataService.TransitionOverlays[(int)LevelTransitions.Intro].Draw(graphicsDevice, spriteBatch);
                    break;
                case LevelState.Play:
                    break;
                case LevelState.Death:
                    commonDataService.TransitionOverlays[(int)LevelTransitions.Death].Draw(graphicsDevice, spriteBatch);
                    break;
                case LevelState.DeathReset:
                    commonDataService.TransitionOverlays[(int)LevelTransitions.DeathReset].Draw(graphicsDevice, spriteBatch);
                    break;
                case LevelState.Outro:
                    {
                        int outroIndex = this.LevelData.IsFinale ? (int)LevelTransitions.FinaleOutro : (int)LevelTransitions.Outro;

                        commonDataService.TransitionOverlays[outroIndex].Draw(graphicsDevice, spriteBatch);
                    }
                    break;
                case LevelState.OutroCompleted:
                    {
                        int outroIndex = this.LevelData.IsFinale ? (int)LevelTransitions.FinaleOutro : (int)LevelTransitions.Outro;

                        commonDataService.TransitionOverlays[outroIndex].Draw(graphicsDevice, spriteBatch);
                    }
                    break;
                default:
                    break;
            }
        }

        public void Stop()
        {
        }

        public void Dispose()
        {
            //this._tileMap.Dispose();
        }

        private void UpdateTransition(GameTime gameTime, ITransitionOverlay[] transitionOverlays, LevelTransitions transition, LevelState nextState)
        {
            ITransitionOverlay transitionOverlay = transitionOverlays[(int)transition];
            transitionOverlay.Update(gameTime);

            if (transitionOverlay.IsCompleted)
            {
                SetNextState(nextState);
            }
        }

        private void SetNextState(LevelState state)
        {
            ILevelCommonDataService commonDataService = this._serviceProvider.GetService<ILevelCommonDataService>();
            IMediaPlayerService mediaPlayer = this._serviceProvider.GetService<IMediaPlayerService>();

            switch (state)
            {
                case LevelState.Intro:
                    {
                        commonDataService.TransitionOverlays[(int)LevelTransitions.Intro].Start();
                        mediaPlayer.Play(commonDataService.BackgroundMusic, true, 0.05f);
                    }
                    break;
                case LevelState.Play:
                    break;
                case LevelState.Death:
                    {
                        commonDataService.TransitionOverlays[(int)LevelTransitions.Death].Start();
                        mediaPlayer.Stop(false);

                        IPlayerManager playerManager = this._serviceProvider.GetService<IPlayerManager>();

                        IInputManager inputManager = this._serviceProvider.GetService<IInputManager>();

                        foreach (IPlayer player in playerManager.Players)
                        {
                            if (player.IsInLevel == false || player.IsDead == false)
                            {
                                continue;
                            }

                            inputManager.GetGamePad(player.Index).SetVibration(new VibrationSettings()
                            {
                                Duration = 0.5f,
                                LeftMotor = 0.75f,
                                RightMotor = 0.75f,
                                Priority = VibrationPriority.High,
                            });
                        }

                        commonDataService.LevelFailedSound.Play(0.4f, 0.0f, 0.0f);
                    }
                    break;
                case LevelState.DeathReset:
                    {
                        commonDataService.TransitionOverlays[(int)LevelTransitions.DeathReset].Start();

                        this._tileMap.Reset();

                        IPlayerManager playerManager = this._serviceProvider.GetService<IPlayerManager>();
                        playerManager.ResetForLevel();

                        mediaPlayer.Play(commonDataService.BackgroundMusic, true, 0.05f);
                    }
                    break;
                case LevelState.Outro:
                    {
                        if (this.LevelData.IsFinale == true)
                        {
                            mediaPlayer.Stop(false);
                            mediaPlayer.Play(commonDataService.FinaleMusic, false, 0.1f);
                        }
                        else
                        {
                            mediaPlayer.Stop(true);
                            commonDataService.LevelCompleteSound.Play(0.4f, 0.0f, 0.0f);
                        }

                        int outroIndex = this.LevelData.IsFinale ? (int)LevelTransitions.FinaleOutro : (int)LevelTransitions.Outro;
                        commonDataService.TransitionOverlays[outroIndex].Start();
                    }
                    break;
                case LevelState.OutroCompleted:
                    break;
                default:
                    break;
            }

            this.State = state;
        }
    }
}
