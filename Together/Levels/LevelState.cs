﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Together.Levels
{
    public enum LevelState
    {
        None,
        Intro,
        Play,
        Death,
        DeathReset,
        Outro,
        OutroCompleted
    }
}
