﻿
namespace Together.Levels.Tiles
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Together.Graphics;
    using Together.Scenes;
    using Together.Schema;

    public class BarrierBlock : Block
    {
        private const int TOTAL_FLICKER_COUNT = 4;
        private const float MIN_FLICKER_ALPHA = 0.05f;
        private const float MAX_FLICKER_ALPHA = 0.6f;
        private const float FLICKER_SPEED = 2.0f;

        private BarrierBlockData _blockData;
        private float _alpha;
        private int _flickerCount;
        private bool _direction;

        public BarrierBlock(Vector2 position, BarrierBlockData data)
            : base(position, data)
        {
            this._blockData = data;
            this.Reset();
        }

        public override void LoadContent(ContentManager contentManager)
        {
            Texture2D blockTexture = contentManager.Load<Texture2D>(this._blockData.Image);

            this._sprite = new AnimatedSprite(new Texture2D[] { blockTexture }, Rectangle.Empty, 0.0f);
        }

        public override void Update(GameTime gameTime, float scale, float tileScale, bool isPlaying)
        {
            base.Update(gameTime, scale, tileScale, isPlaying);

            if (isPlaying == false || this.IsCollidable == true || this.IsDrawable == false)
            {
                return;
            }

            if (SceneViewport.ComputeDirectionalFloat(ref this._alpha, ref this._direction, MIN_FLICKER_ALPHA, MAX_FLICKER_ALPHA, FLICKER_SPEED, (float)gameTime.ElapsedGameTime.TotalSeconds) == true)
            {
                this._flickerCount += 1;
            }

            if (this._flickerCount > TOTAL_FLICKER_COUNT)
            {
                this.IsDrawable = false;
            }

            this._color = new Color(Color.White, this._alpha);
        }

        public override void Reset()
        {
            base.Reset();

            this._alpha = MAX_FLICKER_ALPHA;
            this._flickerCount = 0;
            this._direction = false;
        }

        public void Unlock()
        {
            this.IsCollidable = false;
        }
    }
}
