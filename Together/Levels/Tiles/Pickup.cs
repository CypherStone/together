﻿
namespace Together.Levels.Tiles
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using Together.Graphics;
    using Together.Schema;

    public class Pickup : Tile
    {
        private PickupData _pickupData;
        private Vector2 _startPosition;
        private bool _direction;

        public Pickup(Vector2 position, PickupData pickupData) : base(position, pickupData)
        {
            this._startPosition = position;
            this._pickupData = pickupData;

            this._direction = new Random().Next(0, 2) == 0;
        }

        public override void LoadContent(ContentManager contentManager)
        {
            Texture2D blockTexture = contentManager.Load<Texture2D>(this._pickupData.Image);

            this._sprite = new AnimatedSprite(new Texture2D[] { blockTexture }, Rectangle.Empty, 0.0f);
        }

        public override void Update(GameTime gameTime, float scale, float tileScale, bool isPlaying)
        {
            if (isPlaying == false)
            {
                base.Update(gameTime, scale, tileScale, isPlaying);
                return;
            }

            float movementSpeed = (float)gameTime.ElapsedGameTime.TotalSeconds * 4.0f;

            if (this._direction)
            {
                this.Position = new Vector2(this.Position.X, MathHelper.Lerp(this.Position.Y, this.Position.Y + 0.25f, movementSpeed));
            }
            else
            {
                this.Position = new Vector2(this.Position.X, MathHelper.Lerp(this.Position.Y, this.Position.Y - 0.25f, movementSpeed));
            }

            if (Math.Abs(this._startPosition.Y - this.Position.Y) >= 0.25)
            {
                this._direction = !this._direction;
            }

            base.Update(gameTime, scale, tileScale, isPlaying);
        }

        public override void Reset()
        {
            base.Reset();

            this.Position = this._startPosition;
        }

        public void OnCollected()
        {
            this.IsDrawable = false;
            this.IsCollidable = false;
        }
    }
}
