﻿namespace Together.Levels.Tiles
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Together.Graphics;
    using Together.Schema;

    public class DeathBlock : Tile
    {
        private DeathBlockData _deathBlockData;

        public DeathBlock(Vector2 position, DeathBlockData deathBlockData) : base(position, deathBlockData)
        {
            this._deathBlockData = deathBlockData;
        }

        public override void LoadContent(ContentManager contentManager)
        {
            Texture2D blockTexture = contentManager.Load<Texture2D>(this._deathBlockData.Image);

            this._sprite = new AnimatedSprite(new Texture2D[] { blockTexture }, Rectangle.Empty, 0.0f);
        }
    }
}
