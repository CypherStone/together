﻿
namespace Together.Levels.Tiles
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using System.Linq.Expressions;
    using Together.Graphics;
    using Together.Scenes;
    using Together.Schema;
    using Together.Services;

    public class Tile : IDisposable
    {
        protected TileData _data;
        protected AnimatedSprite _sprite;
        protected Color _color;

        public Vector2 Position { get; protected set; }
        public Rectangle Bounds { get; private set; }
        public bool IsDrawable { get; protected set; }
        public bool IsCollidable { get; protected set; }

        public Tile(Vector2 position, TileData data)
        {
            this.Position = position;
            this._data = data;
            this.IsDrawable = true;
            this.IsCollidable = data.IsCollidable;
            this._color = Color.White;
        }

        public virtual void LoadContent(ContentManager contentManager)
        {
        }

        public virtual void Update(GameTime gameTime, float scale, float tileScale, bool isPlaying)
        {
            this._sprite?.Update(gameTime);

            Texture2D texture = this._sprite?.CurrentTexture;

            Vector2 size;

            if (this._data.NoTileScaling == true && texture != null)
            {
                size = new Vector2(texture.Width, texture.Height) * scale;
            }
            else
            {
                size = new Vector2(tileScale, tileScale);
            }

            size *= this._data.Scale;

            Vector2 position = this.Position * tileScale;

            this.Bounds = new Rectangle(new Point((int)position.X, (int)position.Y), new Point((int)size.X, (int)size.Y));
        }

        public virtual void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch)
        {
            if (this._sprite == null)
            {
                return;
            }

            this._sprite.Color = this._color;
            this._sprite.Rectangle = this.Bounds;
            this._sprite.Draw(spriteBatch);
        }

        public bool DetectCollision(Vector2 position, Vector2 size)
        {
            bool xCollided = Math.Abs(this.Bounds.X - Math.Round(position.X)) * 2 < (this.Bounds.Width + Math.Abs(size.X));
            bool yCollided = Math.Abs(this.Bounds.Y - Math.Round(position.Y)) * 2 < (this.Bounds.Height + Math.Abs(size.Y));

            return xCollided && yCollided;
        }

        public virtual void Reset()
        {
            this._color = Color.White;

            this.IsDrawable = true;
            this.IsCollidable = this._data.IsCollidable;

            this._sprite?.Reset();
        }

        public void Dispose()
        {
            //this._sprite?.Dispose();
        }
    }
}
