﻿namespace Together.Levels.Tiles
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Together.Graphics;
    using Together.Schema;

    public class Block : Tile
    {
        private BlockData _blockData;

        public Block(Vector2 position, BlockData data) 
            : base(position, data)
        {
            this._blockData = data;
        }

        public override void LoadContent(ContentManager contentManager)
        {
            Texture2D blockTexture = contentManager.Load<Texture2D>(this._blockData.Image);

            this._sprite = new AnimatedSprite(new Texture2D[] { blockTexture }, Rectangle.Empty, 0.0f);
        }
    }
}
