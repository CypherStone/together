﻿namespace Together.Levels.Tiles
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Together.Graphics;
    using Together.Scenes;
    using Together.Schema;

    public class ExitBlock : Block
    {
        private ExitBlockData _blockData;
        private float _speed;
        private float _value;
        private float _minValue;
        private float _maxValue;
        private bool _direction;
        private bool _startingDirection;

        public ExitBlock(Vector2 position, ExitBlockData data)
            : base(position, data)
        {
            this._blockData = data;

            if (this._blockData.IsCenter == true)
            {
                this._startingDirection = new System.Random().Next(0, 2) == 0;
                this._speed = SceneViewport.GetRandomNumber(0.2f, 0.8f);
                this._minValue = SceneViewport.GetRandomNumber(0.3f, 0.6f);
                this._maxValue = SceneViewport.GetRandomNumber(this._minValue, 1.1f);
            }
            else
            {
                this._speed = 0.25f;
                this._minValue = 0.2f;
                this._maxValue = 0.6f;
            }

            this.Reset();
        }

        public override void LoadContent(ContentManager contentManager)
        {
            Texture2D blockTexture = contentManager.Load<Texture2D>(this._blockData.Image);

            this._sprite = new AnimatedSprite(new Texture2D[] { blockTexture }, Rectangle.Empty, 0.0f);
        }

        public override void Update(GameTime gameTime, float scale, float tileScale, bool isPlaying)
        {
            base.Update(gameTime, scale, tileScale, isPlaying);

            if (isPlaying == false)
            {
                return;
            }

            SceneViewport.ComputeDirectionalFloat(ref this._value, ref this._direction, this._minValue, this._maxValue, this._speed, (float)gameTime.ElapsedGameTime.TotalSeconds);

            if (this._blockData.IsCenter == true)
            {
                this._color = new Color(0.0f, this._value, 0.0f);
            }
            else
            {
                this._color = new Color(this._value, this._value, this._value);
            }
        }

        public override void Reset()
        {
            base.Reset();

            this._direction = this._startingDirection;
            this._value = this._direction ? this._minValue : this._maxValue;

            this._color = this._blockData.IsCenter ? new Color(0, this._value, 0.0f) : new Color(this._value, this._value, this._value);
        }
    }
}
