﻿
namespace Together.Levels.Tiles
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Together.Game.Character;
    using Together.Scenes;
    using Together.Schema;
    using Together.Services;

    public class TileMap : IDisposable
    {
        private const int COLLISION_DETECTION_STEPS = 5;

        private readonly LevelData _levelData;
        private readonly IGameViewportService _viewportService;
        private readonly ILevelCommonDataService _commonDataService;
        private Tile[,] _tiles;
        private int _totalPickups;
        private bool _unlockedBarriers;

        public Vector2?[] PlayerStarts { get; private set; }

        public TileMap(LevelData levelData, IGameViewportService viewportService, ILevelCommonDataService commonDataService)
        {
            this._levelData = levelData;
            this._viewportService = viewportService;
            this._commonDataService = commonDataService;
            this._tiles = new Tile[TileMapConstants.MapWidth, TileMapConstants.MapHeight];
            this.PlayerStarts = new Vector2?[PlayerManager.MAX_PLAYERS];
        }

        public void LoadContent(ContentManager contentManager)
        {
            contentManager.RootDirectory = "Content/Levels";

            Texture2D levelMapTexture = contentManager.Load<Texture2D>(this._levelData.TileMap);

            contentManager.RootDirectory = "Content/Levels/Tiles";

            Color[] colors = new Color[levelMapTexture.Width * levelMapTexture.Height];

            levelMapTexture.GetData(colors);

            for (int i = 0; i < TileMapConstants.MapWidth; ++i)
            {
                for (int j = 0; j < TileMapConstants.MapHeight; ++j)
                {
                    int pixelIndex = i + (TileMapConstants.MapWidth * j);

                    Color tileColor = colors[pixelIndex];

                    if (tileColor == Color.Black)
                    {
                        continue;
                    }

                    if (this._commonDataService.TilesDict.TryGetValue(tileColor, out TileData tileData) == true)
                    {
                        Tile tile = null;
                        Vector2 position = new Vector2(i, j);

                        if (tileData is PickupData pickupData)
                        {
                            tile = new Pickup(position, pickupData);
                            this._totalPickups += 1;
                        }
                        else if (tileData is ExitBlockData exitBlockData)
                        {
                            tile = new ExitBlock(position, exitBlockData);
                        }
                        else if (tileData is DeathBlockData deathBlockData)
                        {
                            tile = new DeathBlock(position, deathBlockData);
                        }
                        else if (tileData is BarrierBlockData barrierBlockData)
                        {
                            tile = new BarrierBlock(position, barrierBlockData);
                        }
                        else if (tileData is BlockData blockData)
                        {
                            tile = new Block(position, blockData);
                        }
                        else if (tileData is PlayerStartData playerStart)
                        {
                            if (this.PlayerStarts[playerStart.Index] == null)
                            {
                                this.PlayerStarts[playerStart.Index] = position;
                            }
                        }

                        if (tile != null)
                        {
                            tile.LoadContent(contentManager);

                            this._tiles[i, j] = tile;
                        }
                    }
                }
            }

            levelMapTexture.Dispose();

            contentManager.RootDirectory = "Content";
        }

        public void Update(GameTime gameTime, IPlayerManager playerManager, bool isPlaying)
        {
            IPlayer[] players = playerManager.Players.Where(p => p.IsInLevel).ToArray();

            for (int i = 0; i < TileMapConstants.MapWidth; ++i)
            {
                for (int j = 0; j < TileMapConstants.MapHeight; ++j)
                {
                    Tile tile = this._tiles[i, j];

                    if (tile == null)
                    {
                        continue;
                    }

                    tile.Update(gameTime, this._viewportService.Scale, this._viewportService.TileScale, isPlaying);

                    if (tile.IsCollidable == false)
                    {
                        continue;
                    }

                    if (isPlaying == false)
                    {
                        continue;
                    }

                    foreach (IPlayer player in players)
                    {
                        if (Math.Abs(player.VelocityX) > 0.0f || Math.Abs(player.VelocityY) > 0.0f)
                        {
                            float stepVelocityX = Math.Abs(player.VelocityX) > 0.0f ? ((player.VelocityX) * (float)gameTime.ElapsedGameTime.TotalSeconds) / COLLISION_DETECTION_STEPS : 0.0f;
                            float stepVelocityY = Math.Abs(player.VelocityY) > 0.0f ? (-(player.VelocityY) * (float)gameTime.ElapsedGameTime.TotalSeconds) / COLLISION_DETECTION_STEPS : 0.0f;

                            bool isXCollision = false;
                            bool isYCollision = false;

                            Vector2 futurePosition = new Vector2();

                            for (int k = 1; k <= COLLISION_DETECTION_STEPS; ++k)
                            {
                                if (Math.Abs(player.VelocityX) > 0.0f)
                                {
                                    float velocityX = stepVelocityX * (float)k;

                                    futurePosition = player.Position + new Vector2(velocityX, 0.0f);

                                    isXCollision = tile.DetectCollision(futurePosition, new Vector2(player.Bounds.Width, player.Bounds.Height));
                                    if (isXCollision == true)
                                    {
                                        player.OnTileXCollision(tile);
                                    }
                                }

                                if (tile.IsCollidable == false)
                                {
                                    // If we have collided already, don't retest if the tile disables itself.
                                    break;
                                }

                                if (Math.Abs(player.VelocityY) > 0.0f)
                                {
                                    float velocityY = stepVelocityY * (float)k;

                                    futurePosition = player.Position + new Vector2(0.0f, velocityY);

                                    isYCollision = tile.DetectCollision(futurePosition, new Vector2(player.Bounds.Width, player.Bounds.Height));
                                    if (isYCollision == true)
                                    {
                                        player.OnTileYCollision(tile);
                                    }
                                }

                                if (isXCollision || isYCollision)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            int totalPickupsCollected = 0;

            foreach (IPlayer player in players)
            {
                if (player.Position.Y >= this._viewportService.Height + (player.Bounds.Height + 10.0f))
                {
                    // Death bounds
                    player.TakeDamage();
                }

                totalPickupsCollected += player.PickupsCollected;
            }

            if (totalPickupsCollected >= this._totalPickups && this._unlockedBarriers == false)
            {
                for (int i = 0; i < TileMapConstants.MapWidth; ++i)
                {
                    for (int j = 0; j < TileMapConstants.MapHeight; ++j)
                    {
                        if (this._tiles[i, j] is BarrierBlock barrier)
                        {
                            barrier.Unlock();
                        }
                    }
                }

                this._commonDataService.PickupsCollectedSound.Play(0.2f, 0.0f, 0.0f);
                
                this._unlockedBarriers = true;
            }
        }

        public void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);

            for (int i = 0; i < TileMapConstants.MapWidth; ++i)
            {
                for (int j = 0; j < TileMapConstants.MapHeight; ++j)
                {
                    Tile tile = this._tiles[i, j];

                    if (tile == null || tile.IsDrawable == false)
                    {
                        continue;
                    }

                    tile.Draw(graphicsDevice, spriteBatch);
                }
            }

            spriteBatch.End();
        }

        public void Reset()
        {
            for (int i = 0; i < TileMapConstants.MapWidth; ++i)
            {
                for (int j = 0; j < TileMapConstants.MapHeight; ++j)
                {
                    this._tiles[i, j]?.Reset();
                }
            }

            this._unlockedBarriers = false;
        }

        public void Dispose()
        {
            //for (int i = 0; i < TileMapConstants.MapWidth; ++i)
            //{
            //    for (int j = 0; j < TileMapConstants.MapHeight; ++j)
            //    {
            //        this._tiles[i, j]?.Dispose();
            //    }
            //}
        }
    }
}
