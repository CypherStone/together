﻿
namespace Together.Levels
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using Together.Graphics;
    using Together.Scenes;

    public class LevelBackground
    {
        private AnimatedSprite _animatedSprite;

        public void LoadContent(ContentManager contentManager)
        {
            Texture2D[] backgroundTextures = new Texture2D[57];

            for (int i = 0; i < backgroundTextures.Length; ++i)
            {
                backgroundTextures[i] = contentManager.Load<Texture2D>($"Levels/Background/background_{i}");
            }

            this._animatedSprite = new AnimatedSprite(backgroundTextures, Rectangle.Empty, 8.0f / backgroundTextures.Length);
            this._animatedSprite.Start();
            this._animatedSprite.Loop = true;
        }

        public void Update(GameTime gameTime)
        {
            this._animatedSprite.Update(gameTime);
        }

        public void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp);

            Texture2D currentSpriteTexture = this._animatedSprite.CurrentTexture;
            if (SceneViewport.ComputeTextureSize(graphicsDevice, currentSpriteTexture, false, (float)graphicsDevice.Viewport.Width / (float)currentSpriteTexture.Width, out Vector2 textureSize))
            {
                int widthPosition = (graphicsDevice.Viewport.Width / 2) - (int)(textureSize.X / 2);
                int heightPosition = (graphicsDevice.Viewport.Height / 2) - (int)(textureSize.Y / 2);

                this._animatedSprite.Rectangle = new Rectangle(widthPosition, heightPosition, (int)textureSize.X, (int)textureSize.Y);
                this._animatedSprite.Draw(spriteBatch);
            }

            spriteBatch.End();
        }
        public void Reset()
        {
            this._animatedSprite.Reset();
            this._animatedSprite.Start();
        }
    }
}
