﻿
namespace Together.Services
{
    using Microsoft.Xna.Framework;
    using Together.Game.Input;

    public interface IInputManager
    {
        public GamePad GetGamePad(PlayerIndex playerIndex);

    }
}
