﻿namespace Together.Services
{
    using Microsoft.Xna.Framework;

    public interface IGameViewportService
    {
        public int Width { get; }
        public int Height { get; }
        public int HalfWidth { get; }
        public int HalfHeight { get; }
        public float Scale { get; }

        float TileScale { get; }
    }
}
