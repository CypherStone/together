﻿
namespace Together.Services
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Together.Game.Character;

    public interface IPlayerManager
    {
        IPlayer[] Players { get; }

        void LoadContent(ContentManager contentManager);

        void Update(GameTime gameTime);

        void PostUpdate(GameTime gameTime);

        void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice);

        void ResetForLevel();

    }
}
