﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;
using Together.Levels;
using Together.Schema;
using Together.TransitionOverlay;

namespace Together.Services
{
    public interface ILevelCommonDataService
    {
        SpriteFont Font { get; }
        ITransitionOverlay[] TransitionOverlays { get; }
        LevelBackground Background { get; }

        Song BackgroundMusic { get; }
        Song FinaleMusic { get; }

        SoundEffect AcceptSound { get; }
        SoundEffect JumpSound { get; }
        SoundEffect BumpSound { get; }
        SoundEffect PickupSound { get; }
        SoundEffect PickupsCollectedSound { get; }
        SoundEffect DeathSound { get; }
        SoundEffect WarpSound { get; }
        SoundEffect LevelCompleteSound { get; }
        SoundEffect LevelFailedSound { get; }

        Dictionary<Color, TileData> TilesDict { get; }

        float PlayerMovementSpeed { get; }
        float PlayerMaxMovementSpeed { get; }
        float PlayerGravity { get; }
        float PlayerFriction { get; }
        float PlayerJumpVelocity { get; }

        void LoadContent(ContentManager contentManager);

        void Update(GameTime gameTime);

        void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch);

        void Reset();
    }
}
