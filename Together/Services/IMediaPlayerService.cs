﻿namespace Together.Services
{
    using Microsoft.Xna.Framework.Media;

    public interface IMediaPlayerService
    {
        public void Play(Song song, bool loop, float volume);
        public void Stop(bool fadeOut);
    }
}
