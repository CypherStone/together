﻿
namespace Together.Graphics
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using System;

    public class AnimatedSprite : IDisposable
    {
        #region Fields
        private readonly Texture2D[] _textures;
        private float _duration;
        private float _timer;
        private int _index;
        #endregion

        #region Properties
        public Texture2D CurrentTexture => this._textures[this._index];
        public Rectangle Rectangle { get; set; }
        public Color Color { get; set; }
        public float Rotation { get; set; }
        public float Scale { get; set; }
        public bool Loop { get; set; }
        public bool Reverse { get; set; }
        public bool IsPlaying { get; private set; }
        public bool IsHiddenWhenStopped { get; set; }
        #endregion

        public AnimatedSprite(Texture2D[] textures, Rectangle rectangle, float duration)
        {
            this.Rectangle = rectangle;
            this._textures = textures;
            this._duration = duration;

            this.Color = Color.White;
            this.Scale = 0.0f;
        }

        public void Start()
        {
            if (this.IsPlaying == true)
            {
                return;
            }

            this.IsPlaying = true;
            this._timer = 0.0f;
        }

        public void Update(GameTime gameTime)
        {
            if (this.IsPlaying == false)
            {
                return;
            }

            this._timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (this._timer < this._duration)
            {
                return;
            }

            int direction = this.Reverse ? -1 : 1;
            int nextIndex = this._index + direction;

            if (nextIndex < 0 || nextIndex >= this._textures.Length)
            {
                if (this.Loop == false)
                {
                    this.Stop();
                }
                else
                {
                    this._index = nextIndex < 0 ? this._textures.Length - 1 : 0;
                }
            }
            else
            {
                this._index = nextIndex;
            }

            this._timer = 0.0f;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (this.IsPlaying == false && this.IsHiddenWhenStopped == true)
            {
                return;
            }

            spriteBatch.Draw(this._textures[this._index], 
                            this.Rectangle, 
                            null, 
                            this.Color, 
                            this.Rotation, 
                            new Vector2(this.Scale, this.Scale),
                            SpriteEffects.None, 
                            0.0f);
        }

        public void Stop()
        {
            this.IsPlaying = false;
            this._timer = 0.0f;
        }

        public void Reset()
        {
            this.Stop();
            this._index = 0;
        }

        public void Dispose()
        {
            for (int i = 0; i < this._textures.Length; ++i)
            {
                this._textures[i].Dispose();
            }
        }
    }
}
