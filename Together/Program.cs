﻿using System;

namespace Together
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (TogetherGame game = new TogetherGame())
            {
                game.Run();
            }
        }
    }
}
