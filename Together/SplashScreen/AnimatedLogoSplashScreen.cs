﻿
namespace Together.SplashScreen
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using Together.Graphics;
    using Together.Scenes;
    using Together.Services;

    public class AnimatedLogoSplashScreen : IDisposable
    {
        #region Enum
        private enum States
        {
            Starting,
            FadeIn,
            Wait,
            FadeOut,
            Stopping,
            Stopped
        }
        #endregion

        #region Fields
        private readonly IGameViewportService _viewportService;
        private readonly AnimatedSprite _sprite;
        private readonly int _fadeInDuration;
        private readonly int _waitDuration;
        private readonly int _fadeOutDuration;
        private float _colorMultiplier;
        private float _timer;
        private States _state;
        #endregion

        #region Properties
        public bool IsComplete => this._state == States.Stopped;    
        public float Scale { get; set; }
        public bool UseSafeZone { get; set; }
        #endregion

        public AnimatedLogoSplashScreen(Texture2D[] textures, IGameViewportService viewportService, int fadeInDuration = 1, int waitDuration = 2, int fadeOutDuration = 1)
        {
            this._sprite = new AnimatedSprite(textures, Rectangle.Empty, (float)waitDuration / textures.Length);
            this._viewportService = viewportService;
            this._fadeInDuration = fadeInDuration;
            this._waitDuration = waitDuration;
            this._fadeOutDuration = fadeOutDuration;

            this.Scale = 1.0f;
            this._colorMultiplier = 0.0f;
            this._state = States.Starting;
            this._timer = 0.0f;
        }

        public void Update(GameTime gameTime)
        {
            this._timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            int duration;

            switch (this._state)
            {
                case States.Starting:
                    duration = this._waitDuration;
                    break;
                case States.FadeIn:
                    {
                        duration = this._fadeInDuration;
                        this._colorMultiplier += (1.0f / (float)duration) * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    }
                    break;
                case States.Wait:
                    {
                        duration = this._waitDuration;
                        this._sprite.Update(gameTime);
                    }
                    break;
                case States.FadeOut:
                    {
                        duration = this._fadeOutDuration;
                        this._colorMultiplier -= (1.0f / duration) * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    }
                    break;
                case States.Stopping:
                    duration = this._waitDuration;
                    break;
                case States.Stopped:
                    this._timer = 0.0f;
                    return;
                default:
                    break;
            }

            if (this._timer > this._waitDuration)
            {
                this.NextState();
            }
        }

        public void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch)
        {
            if (this._state == States.Starting || this._state == States.Stopped)
            {
                return;
            }

            spriteBatch.Begin();

            Texture2D currentSpriteTexture = this._sprite.CurrentTexture;
            if (SceneViewport.ComputeTextureSize(graphicsDevice, currentSpriteTexture, this.UseSafeZone, this.Scale, out Vector2 textureSize))
            {
                int widthPosition   = this._viewportService.HalfWidth   - (int)(textureSize.X / 2);
                int heightPosition  = this._viewportService.HalfHeight  - (int)(textureSize.Y / 2);

                this._sprite.Color = Color.White * this._colorMultiplier;
                this._sprite.Rectangle = new Rectangle(widthPosition, heightPosition, (int)textureSize.X, (int)textureSize.Y);

                this._sprite.Draw(spriteBatch);
            }

            spriteBatch.End();
        }

        public void Dispose()
        {
            //this._sprite.Dispose();
        }

        private void NextState()
        {
            States nextState = this._state + 1;
            if (nextState == States.Wait)
            {
                this._sprite.Start();
            }
            else if (nextState == States.FadeOut)
            {
                this._sprite.Stop();
            }

            this._state = nextState;

            this._timer = 0.0f;
        }
    }
}
