﻿namespace Together.SplashScreen
{
    using Together.Scenes;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Input;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System.Collections.Generic;
    using Together.Services;
    using Together.Schema;
    using System.Diagnostics;

    public class SplashScreenScene : IScene
    {
        private readonly GameServiceContainer _services;
        private AnimatedLogoSplashScreen _animatedLogoScreen;

        public SceneData Data { get; private set; }

        public IScene NextScene { get; set; }

        public bool IsCompleted { get; private set; }


        public SplashScreenScene(SplashScreenSceneData data, GameServiceContainer services)
        {
            this.Data = data;
            this._services = services;
        }

        public void LoadContent(ContentManager contentManager)
        {
            contentManager.RootDirectory = "Content/SplashScreen";

            SplashScreenSceneData splashScreenData = this.Data as SplashScreenSceneData;

            Debug.Assert(splashScreenData != null);

            IGameViewportService viewportService = this._services.GetService<IGameViewportService>();

            Texture2D[] logoTextures = new Texture2D[splashScreenData.Images.Count];

            for (int i = 0; i < splashScreenData.Images.Count; ++i)
            {
                logoTextures[i] = contentManager.Load<Texture2D>(splashScreenData.Images[i]);
            }

            this._animatedLogoScreen = new AnimatedLogoSplashScreen(logoTextures, viewportService);
            this._animatedLogoScreen.Scale = splashScreenData.Scale;

            contentManager.RootDirectory = "Content";
        }

        public void Start()
        {
        }

        public void UpdateInput(GameTime gameTime)
        {
        }

        public void Update(GameTime gameTime)
        {
            if (this.IsCompleted == true)
            {
                return;
            }

            this._animatedLogoScreen.Update(gameTime);

            this.IsCompleted = this._animatedLogoScreen.IsComplete;
        }

        public void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch)
        {
            graphicsDevice.Clear(Color.Black);

            this._animatedLogoScreen.Draw(graphicsDevice, spriteBatch);
        }

        public void Stop()
        {
        }

        public void Dispose()
        {
            //this._animatedLogoScreen.Dispose();
        }
    }
}
