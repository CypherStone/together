﻿
namespace Together
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using System;
    using Together.Game;
    using Together.Game.Character;
    using Together.Game.Input;
    using Together.Levels;
    using Together.Menu;
    using Together.Scenes;
    using Together.Schema;
    using Together.Services;
    using Together.SplashScreen;
    using Together.TransitionOverlay;

    public class TogetherGame : Microsoft.Xna.Framework.Game
    {
        private readonly GraphicsDeviceManager _graphics;
        private readonly InputManager _inputManager;
        private readonly MediaPlayerService _mediaPlayer;
        private readonly GameViewportService _viewportService;
        private readonly PlayerManager _characterManager;
        private readonly LevelCommonDataService _commonDataService;
        private SpriteBatch _spriteBatch;
        private SceneManager _sceneManager;

        public TogetherGame()
        {
            this.Content.RootDirectory = "Content";

            this.Window.AllowAltF4 = true;
            this.Window.AllowUserResizing = false;

            this._graphics = new GraphicsDeviceManager(this);

#if DEBUG
            this.SetFullscreen(false);
#else
            this.SetFullscreen(true);
#endif // !_DEBUG

            this._inputManager = this.AddService<IInputManager, InputManager>(new InputManager());
            this._mediaPlayer = this.AddService<IMediaPlayerService, MediaPlayerService>(new MediaPlayerService());
            this._viewportService = this.AddService<IGameViewportService, GameViewportService>(new GameViewportService());
            this._commonDataService = this.AddService<ILevelCommonDataService, LevelCommonDataService>(new LevelCommonDataService(this._viewportService));
            this._characterManager = this.AddService<IPlayerManager, PlayerManager>(new PlayerManager(this.Services));

            this._sceneManager = new SceneManager(this.Services);
            //this._splashScreenScene = new SplashScreenScene(this.Services);

            //this._level = new Level(this.Services);
            //this._menuScene = new MenuScene(this.Services);
        }

        protected override void Initialize()
        {
            this.IsMouseVisible = false;

            base.Initialize();

            this.Window.Title = "Together Game";
        }

        protected override void LoadContent()
        {
            this._spriteBatch = new SpriteBatch(this.GraphicsDevice);

            this._characterManager.LoadContent(this.Content);
            this._commonDataService.LoadContent(this.Content);

            this._sceneManager.LoadContent(this.Content);

            this._viewportService.Update(this.GraphicsDevice);
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (Keyboard.GetState().IsKeyDown(Keys.F12) == true)
            {
                this.SetFullscreen(!this._graphics.IsFullScreen);
            }

            this._viewportService.Update(this.GraphicsDevice);
            this._inputManager.Update(gameTime);
            this._mediaPlayer.Update(gameTime);

            this._sceneManager.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            this._sceneManager.Draw(this.GraphicsDevice, this._spriteBatch);
        }

        private void SetFullscreen(bool isFullscreen)
        {
            if (isFullscreen == true)
            {
                this._graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
                this._graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
                this._graphics.IsFullScreen = true;
            }
            else
            {
                this._graphics.PreferredBackBufferWidth = SceneViewport.DefaultWidth;
                this._graphics.PreferredBackBufferHeight = SceneViewport.DefaultHeight;
                this._graphics.IsFullScreen = false;
            }

            this._graphics.ApplyChanges();
        }

        private I AddService<T, I>(I item) where T : class where I : T
        {
            this.Services.AddService<T>(item);
            return item;
        }
    }
}
