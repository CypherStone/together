﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Together.Levels.Tiles;
using Together.Services;

namespace Together.Game.Character
{
    public interface IPlayer
    {
        public PlayerIndex Index { get; }
        public Vector2 Position { get; }
        public Vector2 StartPosition { get; }
        public Rectangle Bounds { get; }

        public float VelocityX { get; }
        public float VelocityY { get; }

        public int PickupsCollected { get; }

        public bool IsDead { get; }
        public bool IsInLevel { get; }
        public bool HasExited { get; }

        public void LoadContent(ContentManager contentManager);

        public void UpdateInput(IInputManager inputManager, GameTime gameTime);

        public void Update(GameTime gameTime);

        public void PostUpdate(GameTime gameTime);

        public void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch);

        public void TakeDamage();

        public void OnTileXCollision(Tile tile);

        public void OnTileYCollision(Tile tile);

        public void SetupForLevel(Vector2? startPosition);

        public void Reset();
    }
}
