﻿
namespace Together.Game.Character
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using Together.Services;

    public class PlayerManager : IPlayerManager
    {
        public const int MAX_PLAYERS  = 2;

        private readonly GameServiceContainer _serviceProvider;

        public IPlayer[] Players { get; private set; }

        public PlayerManager(GameServiceContainer serviceProvider)
        {
            this._serviceProvider = serviceProvider;
            this.Players = new IPlayer[MAX_PLAYERS];

            for (int i = 0; i < MAX_PLAYERS; ++i)
            {
                this.Players[i] = new Player((PlayerIndex)i, serviceProvider);
            }
        }

        public void LoadContent(ContentManager contentManager)
        {
            for (int i = 0; i < this.Players.Length; ++i)
            {
                IPlayer player = this.Players[i];

                player.LoadContent(contentManager);
            }
        }

        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < this.Players.Length; ++i)
            {
                IPlayer player = this.Players[i];

                if (player.IsInLevel == false)
                {
                    continue;
                }

                player.Update(gameTime);
            }
        }

        public void PostUpdate(GameTime gameTime)
        {
            for (int i = 0; i < this.Players.Length; ++i)
            {
                IPlayer player = this.Players[i] as IPlayer;

                if (player.IsInLevel == false)
                {
                    continue;
                }

                player.PostUpdate(gameTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice)
        {
            for (int i = 0; i < this.Players.Length; ++i)
            {
                IPlayer player = this.Players[i];

                if (player.IsInLevel == false)
                {
                    continue;
                }

                player.Draw(graphicsDevice, spriteBatch);
            }
        }

        public void ResetForLevel()
        {
            for (int i = 0; i < this.Players.Length; ++i)
            {
                IPlayer player = this.Players[i];

                if (player.IsInLevel == false)
                {
                    continue;
                }

                player.Reset();
            }
        }
    }
}
