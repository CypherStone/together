﻿
namespace Together.Game.Character
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Audio;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using Together.Game.Input;
    using Together.Graphics;
    using Together.Levels.Tiles;
    using Together.Scenes;
    using Together.Schema;
    using Together.Services;

    public class Player : IPlayer
    {
        private readonly GameServiceContainer _servicesProvider;
        private readonly ILevelCommonDataService _commonDataService;
        private AnimatedSprite _sprite;
        private Vector2 _position;
        private float _exitScale;
        private bool _isOnGround;
        private bool _wasOnGround;
        private bool _wallBumped;
        private bool _isExiting;

        public PlayerIndex Index { get; private set; }
        public Vector2 Position => this._position;
        public Vector2 StartPosition { get; private set; }
        public Rectangle Bounds { get; private set; }

        public float VelocityX { get; private set; }
        public float VelocityY { get; private set; }
        public int PickupsCollected { get; private set; }
        public bool IsDead { get; private set; }
        public bool IsInLevel { get; private set; }
        public bool HasExited { get; private set; }

        public Player(PlayerIndex index, GameServiceContainer gameServiceContainer)
        {
            this.Index = index;
            this._servicesProvider = gameServiceContainer;
            this._commonDataService = gameServiceContainer.GetService<ILevelCommonDataService>();
            this.PickupsCollected = 0;
        }

        public void LoadContent(ContentManager contentManager)
        {
            Texture2D temp = contentManager.Load<Texture2D>($"Player/player{(int)this.Index + 1}");

            this._sprite = new AnimatedSprite(new Texture2D[] { temp }, Rectangle.Empty, 1.0f);
        }

        public void Update(GameTime gameTime)
        {
            if (this.IsDead == true)
            {
                return;
            }

            this._sprite.Update(gameTime);

            this.UpdateBounds();

            if (this.IsControllable() == true)
            {
                float frictionStep = this._commonDataService.PlayerFriction * (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (this.VelocityX > 0.0f)
                {
                    this.VelocityX = Math.Max(this.VelocityX - frictionStep, 0.0f);
                }
                else if (this.VelocityX < 0.0f)
                {
                    this.VelocityX = Math.Min(this.VelocityX + frictionStep, 0.0f);
                }

                this.VelocityX = Math.Clamp(this.VelocityX, -this._commonDataService.PlayerMaxMovementSpeed, this._commonDataService.PlayerMaxMovementSpeed);

                this.VelocityY -= this._commonDataService.PlayerGravity * (float)gameTime.ElapsedGameTime.TotalSeconds;
                this.VelocityY = Math.Clamp(this.VelocityY, -this._commonDataService.PlayerGravity, this._commonDataService.PlayerJumpVelocity);

                this._isOnGround = false;
            }
            else
            {
                if (this._isExiting == true)
                {
                    this._exitScale -= (float)gameTime.ElapsedGameTime.TotalSeconds / (float)this._commonDataService.WarpSound.Duration.TotalSeconds;
                    this._exitScale = Math.Clamp(this._exitScale, 0.0f, 1.0f);
                    this.HasExited = this._exitScale == 0.0f;
                }
            }
        }

        public void UpdateInput(IInputManager inputManager, GameTime gameTime)
        {
            if (this.IsControllable() == false)
            {
                return;
            }

            GamePad gamePad = inputManager.GetGamePad(this.Index);

            this.VelocityX +=  this._commonDataService.PlayerMovementSpeed * gamePad.State.ThumbSticks.Left.X;

            if (gamePad.IsButtonPressed(Microsoft.Xna.Framework.Input.Buttons.A) == true)
            {
                this.OnJump();
            }

            if (this._wallBumped)
            {
                gamePad.SetVibration(new VibrationSettings()
                {
                    Duration = 0.1f,
                    LeftMotor = 0.1f,
                    RightMotor = 0.1f,
                    Priority = VibrationPriority.Low
                });

                this._wallBumped = false;
            }

            if (gamePad.IsButtonPressed(Microsoft.Xna.Framework.Input.Buttons.Y) == true)
            {
                this.TakeDamage();
            }
        }

        public void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch)
        {
            IGameViewportService viewportService = this._servicesProvider.GetService<IGameViewportService>();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);

            Rectangle bounds = this.Bounds;

            Texture2D currentSpriteTexture = this._sprite.CurrentTexture;

            int offsetX = (int)((1.0f + currentSpriteTexture.Width) * viewportService.Scale);
            int offsetY = (int)((1.0f + currentSpriteTexture.Height) * viewportService.Scale);

            bounds.X += offsetX;
            bounds.Y += offsetY;

            this._sprite.Rectangle = bounds;
            this._sprite.Draw(spriteBatch);

            spriteBatch.End();
        }

        public void TakeDamage()
        {
            if (this.IsDead == true)
            {
                return;
            }

            this.IsDead = true;

            this._commonDataService.DeathSound.Play(0.2f, 0.0f, 0.0f);
        }

        public void Reset()
        {
            IGameViewportService gameViewportService = this._servicesProvider.GetService<IGameViewportService>();

            this._position = this.StartPosition * gameViewportService.TileScale;
            this.VelocityX = 0.0f;
            this.VelocityY = 0.0f;
            this.PickupsCollected = 0;

            this._exitScale = 1.0f;

            this.IsDead = false;
            this.HasExited = false;

            this._wallBumped = false;
            this._isOnGround = false;
            this._wasOnGround = false;
            this._isExiting = false;

            this._sprite.Reset();

            this.UpdateBounds();
        }

        public void OnTileXCollision(Tile tile)
        {
            if (tile is ExitBlock)
            {
                this.OnExited();
            }
            else if (tile is Block)
            {
                this.VelocityX = this.OnWallBumped(this.VelocityX, true);
            }
            else
            {
                this.OnTileCollision(tile);
            }
        }

        public void OnTileYCollision(Tile tile)
        {
            if (tile is ExitBlock)
            {
                this.OnExited();
            }
            else if (tile is Block)
            {
                this._isOnGround = tile.Bounds.Top > this.Bounds.Bottom;
                this.VelocityY = this.OnWallBumped(this.VelocityY, (this._isOnGround == true && this._wasOnGround == false) || tile.Bounds.Bottom > this.Bounds.Top);
            }
            else
            {
                this.OnTileCollision(tile);
            }
        }

        public void SetupForLevel(Vector2? startPosition)
        {
            this.IsInLevel = startPosition.HasValue;

            if (startPosition.HasValue == true)
            {
                this.StartPosition = startPosition.Value;
            }

            this.Reset();
        }

        public void PostUpdate(GameTime gameTime)
        {
            if (this.IsControllable() == false)
            {
                return;
            }

            this._position.X += this.VelocityX * (float)gameTime.ElapsedGameTime.TotalSeconds;
            this._position.Y += -this.VelocityY * (float)gameTime.ElapsedGameTime.TotalSeconds;

            this._wasOnGround = this._isOnGround;
        }

        private void UpdateBounds()
        {
            Texture2D currentSpriteTexture = this._sprite.CurrentTexture;
            IGameViewportService viewportService = this._servicesProvider.GetService<IGameViewportService>();
            SceneViewport.ComputeScale(viewportService.Width, viewportService.Height, new Vector2(currentSpriteTexture.Width, currentSpriteTexture.Height), PlayerConstants.Scale, out Vector2 scaledSize);

            scaledSize *= this._exitScale;

            Vector2 halfSize = (scaledSize * 0.5f);
            Vector2 position = this._position - halfSize;
            this.Bounds = new Rectangle((int)position.X, (int)position.Y, (int)scaledSize.X, (int)scaledSize.Y);
        }

        private void OnTileCollision(Tile tile)
        {
            if (tile is Pickup pickup)
            {
                this.OnPickupCollected(pickup);
            }
            else if (tile is DeathBlock)
            {
                this.TakeDamage();
            }
        }

        private void OnJump()
        {
            if (this._wasOnGround == false || this._isOnGround == false)
            {
                return;
            }

            this.VelocityY += this._commonDataService.PlayerJumpVelocity;
            this._commonDataService.JumpSound.Play(0.1f, SceneViewport.GetRandomNumber(-0.2f, 0.2f), 0.0f);
        }

        private float OnWallBumped(float velocity, bool playSound)
        {
            this._wallBumped = Math.Abs(velocity) >= this._commonDataService.PlayerMaxMovementSpeed;

            if (playSound == true && this._wallBumped == true)
            {
                this._commonDataService.BumpSound.Play(0.2f, SceneViewport.GetRandomNumber(-0.5f, 0.5f), 0.0f);
            }

            return 0.0f;
        }

        private void OnPickupCollected(Pickup pickup)
        {
            pickup.OnCollected();

            this.PickupsCollected += 1;

            this._commonDataService.PickupSound.Play(0.2f, 0.0f, 0.0f);
        }

        private void OnExited()
        {
            if (this._isExiting == true)
            { 
                return; 
            }

            this._isExiting = true;
            this.VelocityX = 0.0f;
            this.VelocityY = 0.0f;
            this._exitScale = 1.0f;

            this._commonDataService.WarpSound.Play(0.2f, 0.0f, 0.0f);
        }

        private bool IsControllable()
        {
            return this._isExiting == false && this.HasExited == false && this.IsDead == false;
        }
    }
}
