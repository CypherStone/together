﻿
namespace Together.Game.Input
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Input;
    using XGamePad = Microsoft.Xna.Framework.Input.GamePad;

    public class GamePad
    {
        private GamePadState _previousState;
        private GamePadState _state;
        private VibrationSettings _vibrationSettings;
        private PlayerIndex _playerIndex;

        public GamePadState State => this._state;

        private GamePad(PlayerIndex index)
        {
            this._playerIndex = index;
            this.Reset();
        }

        public void Update(GameTime gameTime)
        {
            this._previousState = this._state;
            this._state = XGamePad.GetState(this._playerIndex);

            if (this._state.IsConnected == false)
            {
                this.Reset();
                return;
            }

            if (this._vibrationSettings != null)
            {
                this._vibrationSettings.Timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (this._vibrationSettings.Timer < this._vibrationSettings.Duration)
                {
                    XGamePad.SetVibration(this._playerIndex, this._vibrationSettings.LeftMotor, this._vibrationSettings.RightMotor);
                }
                else
                {
                    XGamePad.SetVibration(this._playerIndex, 0.0f, 0.0f);
                    this._vibrationSettings = null;
                }
            }
        }

        public bool IsButtonPressed(Buttons button)
        {
            return this._previousState.IsButtonUp(button) && this._state.IsButtonDown(button);
        }

        public void SetVibration(VibrationSettings settings)
        {
            if (settings == null)
            {
                return;
            }

            if (this._vibrationSettings == null)
            {
                this._vibrationSettings = settings;
            }
            else if (settings.Priority >= this._vibrationSettings.Priority)
            {
                this._vibrationSettings = settings;
            }
        }

        private void Reset()
        {
            this._vibrationSettings = null;
            this._state = new GamePadState();
        }

        public static GamePad[] CreateGamePads()
        {
            XGamePad.InitDatabase();

            GamePad[] gamePads = new GamePad[XGamePad.MaximumGamePadCount];

            for (int i = 0; i < XGamePad.MaximumGamePadCount; ++i)
            {
                gamePads[i] = new GamePad((PlayerIndex)i);
            }

            return gamePads;
        }
    }
}
