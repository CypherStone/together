﻿
namespace Together.Game.Input
{
    using Microsoft.Xna.Framework;
    using Together.Services;

    public class InputManager : IInputManager
    {
        private GamePad[] _gamePads;

        public InputManager()
        {
            this._gamePads = GamePad.CreateGamePads();

            //VibrationSettings settings = new VibrationSettings();
            //settings.Priority = VibrationPriority.Low;
            //settings.Duration = 10.0f;
            //settings.LeftMotor = 1.0f;
            //settings.RightMotor = 1.0f;

            //GetGamePad(PlayerIndex.One).SetVibration(settings);
        }

        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < this._gamePads.Length; ++i)
            {
                this._gamePads[i].Update(gameTime);
            }
        }

        public GamePad GetGamePad(PlayerIndex index)
        {
            return this._gamePads[(int)index];
        }
    }
}
