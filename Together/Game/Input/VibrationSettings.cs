﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Together.Game.Input
{
    public class VibrationSettings
    {
        public VibrationPriority Priority   { get; set; }
        public float Duration               { get; set; }
        public float Timer                  { get; set; }
        public float LeftMotor             { get; set; }
        public float RightMotor            { get; set; }
    }
}
