﻿namespace Together.Game
{
    using System;
    using Together.Services;

    using Microsoft.Xna.Framework.Media;
    using Microsoft.Xna.Framework;

    public class MediaPlayerService : IMediaPlayerService
    {
        private const float FADE_DURATION = 1.0f;
        private float _currentFade;
        private bool _fadeOut;

        public void Play(Song song, bool loop, float volume = 1.0f)
        {
            if (MediaPlayer.State == MediaState.Playing)
            {
                return;
            }

            MediaPlayer.Play(song);
            MediaPlayer.Volume = volume;
            MediaPlayer.IsRepeating = loop;
            this._fadeOut = false;
        }

        public void Update(GameTime gameTime)
        {
            if (this._fadeOut == false)
            {
                return;
            }

            if (MediaPlayer.State != MediaState.Playing)
            {
                this._fadeOut = false;
                return;
            }    

            float currentVolume = MediaPlayer.Volume - (((float)gameTime.ElapsedGameTime.TotalSeconds / FADE_DURATION) * this._currentFade);
            currentVolume = Math.Clamp(currentVolume, 0.0f, 1.0f);

            MediaPlayer.Volume = currentVolume;

            if (currentVolume == 0.0f)
            {
                MediaPlayer.Stop();
                this._fadeOut = false;
            }
        }

        public void Stop(bool fadeOut)
        {
            this._fadeOut = fadeOut;

            if (fadeOut == false)
            {
                MediaPlayer.Stop();
            }
            else
            {
                this._currentFade = MediaPlayer.Volume;
            }
        }
    }
}
