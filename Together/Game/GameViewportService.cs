﻿namespace Together.Game
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Together.Scenes;
    using Together.Schema;
    using Together.Services;

    public class GameViewportService : IGameViewportService
    {
        public int Width { get; private set; }
        public int Height { get; private set; }
        public int HalfWidth { get; private set; }
        public int HalfHeight { get; private set; }
        public float Scale { get; private set; }
        public float TileScale { get; private set; }

        public void Update(GraphicsDevice graphicsDevice)
        {
            this.Width      = graphicsDevice.Viewport.Width;
            this.Height     = graphicsDevice.Viewport.Height;
            this.HalfWidth  = Width / 2;
            this.HalfHeight = Height / 2;

            this.Scale      = ((float)this.Width / (float)SceneViewport.DefaultWidth);

            this.TileScale  = ((float)this.Width / (float)SceneViewport.DefaultWidth) * (float)(SceneViewport.DefaultWidth / TileMapConstants.MapWidth);
        }
    }
}
