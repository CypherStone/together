﻿namespace Together.Menu
{
    using Together.Scenes;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using Together.Services;
    using Together.Game;
    using Together.Schema;
    using Together.Game.Input;
    using Together.Graphics;

    public class MenuScene : IScene
    {
        private const float MAX_HEART_HEIGHT_OFFSET = 20.0f;

        private GameServiceContainer _serviceProvider;
        private AnimatedSprite _heartSprite;
        private AnimatedSprite _playerOneSprite;
        private AnimatedSprite _playerTwoSprite;
        private AnimatedSprite _startButton;
        private float _alpha;
        private float _alphaTarget;

        private float _heartHeightOffset;
        private float _startButtonFade;
        private bool _heartHeightOffsetDirection;
        private bool _startButtonFadeDirection;

        public SceneData Data { get; private set; }
        public IScene NextScene { get; set; }
        public bool IsCompleted => this._alphaTarget == 0.0f && this._alpha == 0;

        public MenuScene(MenuSceneData data, GameServiceContainer serviceProvider)
        {
            this.Data = data;
            this._serviceProvider = serviceProvider;
            this._alpha = 0.0f;
            this._alphaTarget = 1.0f;
            this._startButtonFade = 1.0f;
        }

        public void LoadContent(ContentManager contentManager)
        {
            this._heartSprite = new AnimatedSprite(new Texture2D[] { contentManager.Load<Texture2D>("Levels/Tiles/Pickup") }, Rectangle.Empty, 0.0f);
            this._playerOneSprite = new AnimatedSprite(new Texture2D[] { contentManager.Load<Texture2D>("Player/Player1") }, Rectangle.Empty, 0.0f);
            this._playerTwoSprite = new AnimatedSprite(new Texture2D[] { contentManager.Load<Texture2D>("Player/Player2") }, Rectangle.Empty, 0.0f);
            this._startButton = new AnimatedSprite(new Texture2D[] { contentManager.Load<Texture2D>("Levels/xbox_a_button") }, Rectangle.Empty, 0.0f);
        }

        public void Start()
        {
            ILevelCommonDataService commonDataService = this._serviceProvider.GetService<ILevelCommonDataService>();
            IMediaPlayerService mediaPlayerService = this._serviceProvider.GetService<IMediaPlayerService>();

            commonDataService.TransitionOverlays[(int)LevelTransitions.Outro].Start();

            mediaPlayerService.Play(commonDataService.BackgroundMusic, true, 0.05f);
        }

        public void UpdateInput(GameTime gameTime)
        {
            if (this._alpha < 0.9f)
            {
                return;
            }

            IInputManager inputManager = this._serviceProvider.GetService<IInputManager>();

            if (inputManager.GetGamePad(PlayerIndex.One).IsButtonPressed(Microsoft.Xna.Framework.Input.Buttons.A) == true)
            {
                inputManager.GetGamePad(PlayerIndex.One).SetVibration(new VibrationSettings()
                {
                    Duration = 0.2f,
                    LeftMotor = 0.4f,
                    RightMotor = 0.4f,
                    Priority = VibrationPriority.Medium
                });

                IMediaPlayerService mediaPlayerService = this._serviceProvider.GetService<IMediaPlayerService>();
                mediaPlayerService.Stop(true);

                ILevelCommonDataService commonDataService = this._serviceProvider.GetService<ILevelCommonDataService>();
                commonDataService.AcceptSound.Play(0.5f, 0.0f, 0.0f);

                this._alphaTarget = 0.0f;
            }
        }

        public void Update(GameTime gameTime)
        {
            ILevelCommonDataService commonDataService = this._serviceProvider.GetService<ILevelCommonDataService>();

            if (commonDataService.TransitionOverlays[(int)LevelTransitions.Outro].IsCompleted == true)
            {
                this._alpha += 0.5f * (this._alphaTarget > 0.0f ? (float)gameTime.ElapsedGameTime.TotalSeconds : (float)-gameTime.ElapsedGameTime.TotalSeconds);
                this._alpha = Math.Clamp(this._alpha, 0.0f, 1.0f);

                if (this._alpha > 0.95f)
               {
                    SceneViewport.ComputeDirectionalFloat(ref this._heartHeightOffset, ref this._heartHeightOffsetDirection, -MAX_HEART_HEIGHT_OFFSET, MAX_HEART_HEIGHT_OFFSET, MAX_HEART_HEIGHT_OFFSET, (float)gameTime.ElapsedGameTime.TotalSeconds);
                    SceneViewport.ComputeDirectionalFloat(ref this._startButtonFade, ref this._startButtonFadeDirection, 0.6f, 1.0f, 0.75f, (float)gameTime.ElapsedGameTime.TotalSeconds);
                }
            }
            else
            {
                commonDataService.TransitionOverlays[(int)LevelTransitions.Outro].Update(gameTime);
            }
        }

        public void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch)
        {
            ILevelCommonDataService commonDataService = this._serviceProvider.GetService<ILevelCommonDataService>();
            IGameViewportService viewportService = this._serviceProvider.GetService<IGameViewportService>();

            if (commonDataService.TransitionOverlays[(int)LevelTransitions.Outro].IsCompleted == true)
            {
                graphicsDevice.Clear(Color.White);

                MenuSceneData menuSceneData = this.Data as MenuSceneData;

                Vector2 titleTextSize = commonDataService.Font.MeasureString(menuSceneData.Title) * 0.5f;
                Vector2 startPrefixTextSize = commonDataService.Font.MeasureString(menuSceneData.StartPrefix) * 0.5f;
                Vector2 startPostfixSize = commonDataService.Font.MeasureString(menuSceneData.StartPostfix) * 0.5f;

                const int padding = 10;
                float startIcon = (padding + this._startButton.CurrentTexture.Width) * viewportService.Scale;
                float startHalfWidthIcon = (this._startButton.CurrentTexture.Width * 0.5f) * viewportService.Scale;
                float startQHeightIcon = this._startButton.CurrentTexture.Height * 0.25f;

                Color textColor = Color.Black;
                textColor.A = (byte)(byte.MaxValue * this._alpha);

                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);

                float titleTextHeight = viewportService.Height * 0.25f;
                float startTextHeight = viewportService.Height * 0.85f;

                spriteBatch.DrawString(commonDataService.Font, menuSceneData.Title, new Vector2(viewportService.HalfWidth, titleTextHeight) - titleTextSize, textColor);
                spriteBatch.DrawString(commonDataService.Font, menuSceneData.StartPrefix, new Vector2(viewportService.HalfWidth - startPostfixSize.X - startIcon, startTextHeight) - startPrefixTextSize, textColor);
                spriteBatch.DrawString(commonDataService.Font, menuSceneData.StartPostfix, new Vector2(viewportService.HalfWidth + startPrefixTextSize.X + startIcon, startTextHeight) - startPostfixSize, textColor);

                this._heartSprite.Color = new Color(Color.White, this._alpha);
                this.CalculateSpritePositionAndSize(this._heartSprite, new Vector2(viewportService.HalfWidth, viewportService.HalfHeight + this._heartHeightOffset), 20.0f, viewportService);
                this._heartSprite.Draw(spriteBatch);

                this._playerOneSprite.Color = new Color(Color.White, this._alpha);
                this.CalculateSpritePositionAndSize(this._playerOneSprite, new Vector2(viewportService.Width * 0.4f, viewportService.Height * 0.65f), PlayerConstants.Scale, viewportService);
                this._playerOneSprite.Draw(spriteBatch);

                this._playerTwoSprite.Color = new Color(Color.White, this._alpha);
                this.CalculateSpritePositionAndSize(this._playerTwoSprite, new Vector2(viewportService.Width * 0.6f, viewportService.Height * 0.65f), PlayerConstants.Scale, viewportService);
                this._playerTwoSprite.Draw(spriteBatch);

                this._startButton.Color = new Color(Color.White, this._alpha * this._startButtonFade);
                this.CalculateSpritePositionAndSize(this._startButton, new Vector2(viewportService.HalfWidth - startHalfWidthIcon, startTextHeight - startQHeightIcon), 1.5f, viewportService);
                this._startButton.Draw(spriteBatch);

                spriteBatch.End();
            }
            else
            {
                graphicsDevice.Clear(Color.Black);

                commonDataService.TransitionOverlays[(int)LevelTransitions.Outro].Draw(graphicsDevice, spriteBatch);
            }
        }

        public void Stop()
        {
        }

        public void Dispose()
        {
        }

        private void CalculateSpritePositionAndSize(AnimatedSprite sprite, Vector2 position, float scale, IGameViewportService viewportService)
        {
            Texture2D currentSpriteTexture = sprite.CurrentTexture;
            SceneViewport.ComputeScale(viewportService.Width, viewportService.Height, new Vector2(currentSpriteTexture.Width, currentSpriteTexture.Height), scale, out Vector2 scaledSize);
            Vector2 halfSize = (scaledSize * 0.5f);
            Vector2 scaledPosition = position - halfSize;
            sprite.Rectangle = new Rectangle((int)scaledPosition.X, (int)scaledPosition.Y, (int)scaledSize.X, (int)scaledSize.Y);
        }
    }
}
