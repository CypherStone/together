﻿namespace Together.Scenes
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using Together.Schema;

    public interface IScene : IDisposable
    {
        public SceneData Data { get; }

        public bool IsCompleted { get; }

        public IScene NextScene { get; set; }

        void LoadContent(ContentManager contentManager);

        void Start();

        void UpdateInput(GameTime gameTime);

        void Update(GameTime gameTime);

        void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch);

        void Stop();
    }
}
