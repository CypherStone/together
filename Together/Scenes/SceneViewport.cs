﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Together.Scenes
{
    public static class SceneViewport
    {
        public const float  SafeAreaRatio       = 0.8f;
        public const int    DefaultWidth        = 1280;
        public const int    DefaultHeight       = 720;
        public const int    SafeDefaultWidth    = (int)(DefaultWidth * SafeAreaRatio);
        public const int    SafeDefaultHeight   = (int)(DefaultHeight * SafeAreaRatio);

        public static bool ComputeTextureSize(GraphicsDevice graphicsDevice, Texture2D texture, bool useSafeArea, float scale, out Vector2 size)
        {
            if (graphicsDevice == null || texture == null)
            {
                size = new Vector2();
                return false;
            }

            float areaRatio      = useSafeArea ? SafeAreaRatio : 1.0f;

            int viewportWidth   = (int)(graphicsDevice.Viewport.Width * areaRatio);
            int viewortHeight   = (int)(graphicsDevice.Viewport.Height * areaRatio);

            ComputeScale(viewportWidth, viewortHeight, new Vector2(texture.Width, texture.Height), scale, out size);

            return true;
        }

        public static void ComputeScale(int viewportWidth, int viewortHeight, Vector2 size, float scale, out Vector2 scaledSize)
        {
            float widthScaler = (float)viewportWidth / (DefaultWidth);
            float heightScaler = (float)viewortHeight / (DefaultHeight);

            scaledSize = new Vector2((size.X * widthScaler) * scale, (size.Y * heightScaler) * scale);
        }

        public static bool ComputeDirectionalFloat(ref float value, ref bool direction, float min, float max, float speed, float delta)
        {
            float target = direction ? 1.0f : -1.0f;

            value += ((target * delta) * speed);
            value = Math.Clamp(value, min, max);

            if (value <= min || value >= max)
            {
                direction = !direction;
                value = direction ? min : max;
                return true;
            }

            return false;
        }

        public static float GetRandomNumber(float minimum, float maximum)
        {
            Random random = new Random();
            return (float)(random.NextDouble() * (maximum - minimum) + minimum);
        }
    }
}
