﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Together.Scenes
{
    public enum SceneFlow
    {
        None,
        Start,
        Update,
        End
    }
}
