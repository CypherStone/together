﻿namespace Together.Scenes
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Together.Finale;
    using Together.Levels;
    using Together.Menu;
    using Together.Schema;
    using Together.Services;
    using Together.SplashScreen;

    public class SceneManager
    {
        private readonly GameServiceContainer _serviceProvider;
        private ContentManager _contentManager;
        private List<IScene> _scenes;
        private IScene _setupScene;
        private IScene _currentScene;
        private SceneFlow _currentSceneFlow;

        public SceneManager(GameServiceContainer serviceProvier)
        {
            this._serviceProvider = serviceProvier;
            this._scenes = new List<IScene>();
        }

        public void LoadContent(ContentManager contentManager)
        {
            this._contentManager = contentManager;

            ScenesData scenesData = contentManager.Load<ScenesData>("Scenes/scenes");

            Dictionary<Guid, IScene> scenesDict = new Dictionary<Guid, IScene>(); ;

            foreach (SceneData sceneData in scenesData.Scenes)
            {
                Guid id = Guid.Parse(sceneData.ID);
                IScene scene = null;

                if (sceneData is SplashScreenSceneData splashScreenData)
                {
                    scene = new SplashScreenScene(splashScreenData, this._serviceProvider);
                }
                else if (sceneData is MenuSceneData menuData)
                {
                    scene = new MenuScene(menuData, this._serviceProvider);
                }
                else if (sceneData is LevelData levelData)
                {
                   scene = new Level(levelData, this._serviceProvider);
                }
                else if (sceneData is FinaleSceneData finaleData)
                {
                    scene = new FinaleScene(finaleData, this._serviceProvider);
                }

                Debug.Assert(scene != null);

                if (scene != null)
                {
                    this._scenes.Add(scene);
                    scenesDict.Add(id, scene);
                }
            }

            foreach (IScene scene in this._scenes)
            {
                Guid nextSceneId = Guid.Parse(scene.Data.NextSceneID);

                if (scenesDict.TryGetValue(nextSceneId, out IScene nextScene) == true)
                {
                    scene.NextScene = nextScene;
                }
            }
        }

        public void Update(GameTime gameTime)
        {
            switch (this._currentSceneFlow)
            {
                case SceneFlow.None:
                    {
                        IInputManager inputManager = this._serviceProvider.GetService<IInputManager>();

                        if (inputManager.GetGamePad(PlayerIndex.One).IsButtonPressed(Microsoft.Xna.Framework.Input.Buttons.A) == true)
                        {
                            if (this._scenes.Count > 0)
                            {
                                ILevelCommonDataService commonDataService = this._serviceProvider.GetService<ILevelCommonDataService>();
                                commonDataService.AcceptSound.Play(0.5f, 0.0f, 0.0f);

                                this.SetNextScene(this._scenes[0]);
                            }
                        }
                    }
                    break;
                case SceneFlow.Start:
                    {
                        this._setupScene.LoadContent(this._contentManager);
                        this._setupScene.Start();
                        this._currentScene = this._setupScene;
                        this._setupScene = null;
                        this._currentSceneFlow = SceneFlow.Update;
                    }
                    break;
                case SceneFlow.Update:
                    {
                        this._currentScene.UpdateInput(gameTime);
                        this._currentScene.Update(gameTime);

                        if (this._currentScene.IsCompleted == true)
                        {
                            this._currentSceneFlow = SceneFlow.End;
                        }
                    }
                    break;
                case SceneFlow.End:
                    {
                        this._currentScene.Stop();
                        //this._currentScene.Dispose();

                        this.SetNextScene(this._currentScene.NextScene);
                    }
                    break;
                default:
                    break;
            }
        }

        public void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch)
        {
            if (this._currentScene == null)
            {
                if (this._currentSceneFlow == SceneFlow.None)
                {
                    graphicsDevice.Clear(Color.Black);
                }
                return;
            }

            this._currentScene.Draw(graphicsDevice, spriteBatch);
        }

        private void SetNextScene(IScene scene)
        {
            if (this._currentScene == scene || this._setupScene == scene)
            {
                return;
            }

            Debug.Assert(this._currentSceneFlow == SceneFlow.None || this._currentSceneFlow == SceneFlow.End);

            this._setupScene = scene;
            this._currentScene = null;
            this._currentSceneFlow = SceneFlow.Start;
        }
    }
}
