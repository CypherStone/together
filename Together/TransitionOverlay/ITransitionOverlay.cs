﻿
namespace Together.TransitionOverlay
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    public interface ITransitionOverlay
    {
        bool IsCompleted { get; }

        void LoadContent(ContentManager contentManager);

        void Start();

        void Update(GameTime gameTime);

        void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch);
    }
}
