﻿namespace Together.TransitionOverlay
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using Together.Graphics;
    using Together.Scenes;
    using Together.Schema;
    using Together.Services;

    public class MatrixPixelTransitionOverlay : ITransitionOverlay, IDisposable
    {
        private readonly IGameViewportService _viewportService;
        private AnimatedSprite _overlaySprite;
        private Texture2D _overlayTexture;

        private MatrixTransitionData _transitionData;

        private Color[] _pixels;
        private int[] _columnCurrentPixel;
        private int[] _columnSpeeds;

        private int _maxWidth;
        private int _maxHeight;

        private bool _shouldUpdate;

        public bool IsCompleted { get; private set; }

        public MatrixPixelTransitionOverlay(IGameViewportService viewportService, MatrixTransitionData data)
        {
            this._viewportService = viewportService;
            this._transitionData = data;
        }

        public void LoadContent(ContentManager contentManager)
        {
            this._overlayTexture = contentManager.Load<Texture2D>("Transitions/" + this._transitionData.Image);

            this._maxWidth = this._overlayTexture.Width;
            this._maxHeight = this._overlayTexture.Height;

            this._overlaySprite = new AnimatedSprite(new Texture2D[] { this._overlayTexture }, Rectangle.Empty, 0.0f);

            this._pixels = new Color[this._overlayTexture.Width * this._overlayTexture.Height];
            this._overlayTexture.GetData<Color>(this._pixels);
        }

        public void Start()
        {
            this._columnCurrentPixel = new int[this._maxWidth];
            this._columnSpeeds = new int[this._maxWidth];

            for (int i = 0; i < this._maxWidth; ++i)
            {
                for (int j = 0; j < this._maxHeight; ++j)
                {
                    int pixelIndex = i + (j * this._maxWidth);

                    this._pixels[pixelIndex].A = this._transitionData.IsReversed ? byte.MaxValue : byte.MinValue;
                }

                this._columnCurrentPixel[i] = this._transitionData.IsReversed ? this._maxHeight - 1 : 0;
            }

            this._overlayTexture.SetData<Color>(this._pixels);

            Random random = new Random();

            for (int i = 0; i < this._maxWidth; ++i)
            {
                this._columnSpeeds[i] = random.Next(this._transitionData.MinSpeed, this._transitionData.MaxSpeed);
            }

            this._shouldUpdate = true;
            this.IsCompleted = false;

            this._overlaySprite.Start();
        }

        public void Update(GameTime gameTime)
        {
            this._overlaySprite.Update(gameTime);

            if (this._shouldUpdate == false)
            {
                return;
            }

            bool hasUpdated = false;

            float direction = this._transitionData.IsReversed ? -1.0f : 1.0f;
            byte maxTarget = this._transitionData.IsReversed ? byte.MinValue : byte.MaxValue;

            for (int i = 0; i < this._maxWidth; ++i)
            {
                int currentPixel = this._columnCurrentPixel[i];

                if (currentPixel < 0)
                {
                    continue;
                }

                int columnIndex = i + (currentPixel * this._maxWidth);
                int currentAlpha = this._pixels[columnIndex].A;
                int newAlpha = currentAlpha + (int)(this._columnSpeeds[i] * gameTime.ElapsedGameTime.TotalSeconds * direction);

                byte clampedAlpha = (byte)Math.Clamp(newAlpha, byte.MinValue, byte.MaxValue);
                this._pixels[columnIndex].A = clampedAlpha;

                if (clampedAlpha == maxTarget)
                {
                    this._columnCurrentPixel[i] += (int)direction;

                    if (this._columnCurrentPixel[i] >= this._maxHeight)
                    {
                        this._columnCurrentPixel[i] = -1;
                    }
                    else
                    {
                        hasUpdated = true;
                    }
                }
                else
                {
                    hasUpdated = true;
                }
            }

            this._overlayTexture.SetData<Color>(this._pixels);

            this._shouldUpdate = hasUpdated;

            if (this._shouldUpdate == false)
            {
                this.IsCompleted = true;
            }
        }

        public void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp);

            Texture2D currentSpriteTexture = this._overlaySprite.CurrentTexture;
            if (SceneViewport.ComputeTextureSize(graphicsDevice, currentSpriteTexture, false, SceneViewport.DefaultWidth / this._maxWidth, out Vector2 textureSize))
            {
                int widthPosition   = this._viewportService.HalfWidth   - (int)(textureSize.X / 2);
                int heightPosition  = this._viewportService.HalfHeight  - (int)(textureSize.Y / 2);

                this._overlaySprite.Rectangle = new Rectangle(widthPosition, heightPosition, (int)textureSize.X, (int)textureSize.Y);

                this._overlaySprite.Draw(spriteBatch);
            }

            spriteBatch.End();
        }

        public void Dispose()
        {
            this._overlaySprite.Dispose();
        }
    }
}
