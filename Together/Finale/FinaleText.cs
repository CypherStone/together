﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Together.Finale
{
    public class FinaleText
    {
        public string Text { get; set; }
        public float Alpha { get; set; }

        public FinaleText()
        {
            this.Alpha = 0.0f;
        }
    }
}
