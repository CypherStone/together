﻿namespace Together.Finale
{
    using Together.Scenes;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using Together.Services;
    using Together.Schema;
    using System.Collections.Generic;

    public class FinaleScene : IScene
    {
        private const float NORMAL_TEXT_FADE_SPEED = 5.0f;
        private const float SLOW_TEXT_FADE_SPEED = 10.0f;

        private readonly GameServiceContainer _serviceProvider;
        private readonly List<FinaleText> _texts;
        private int _overlayIndex;

        public SceneData Data { get; private set; }
        public IScene NextScene { get; set; }
        public bool IsCompleted => false;

        public FinaleScene(FinaleSceneData data, GameServiceContainer serviceProvider)
        {
            this.Data = data;
            this._serviceProvider = serviceProvider;
            this._texts = new List<FinaleText>();
            this._overlayIndex = (int)LevelTransitions.FinaleOutro;
        }

        public void LoadContent(ContentManager contentManager)
        {
            this._texts.Add(new FinaleText() { Text = "Congratulations Kirsty, we have completed TOGETHER!" });
            this._texts.Add(new FinaleText() { Text = "There is a lot we can achieve when we do it together." });
            this._texts.Add(new FinaleText() { Text = "We have been together for a long time now." });
            this._texts.Add(new FinaleText() { Text = "You are my happiness, my world." });
            this._texts.Add(new FinaleText() { Text = "My life wouldn't be the same without you." });
            this._texts.Add(new FinaleText() { Text = "I love you." });
            this._texts.Add(new FinaleText() { Text = "I really want to show that to you." });
            this._texts.Add(new FinaleText() { Text = "So there is just one more thing I could ask from you." });
            this._texts.Add(new FinaleText() { Text = "Will you marry me?" });
        }

        public void Start()
        {
            ILevelCommonDataService commonDataService = this._serviceProvider.GetService<ILevelCommonDataService>();
            IMediaPlayerService mediaPlayer = this._serviceProvider.GetService<IMediaPlayerService>();
            mediaPlayer.Play(commonDataService.FinaleMusic, false, 0.1f);
        }

        public void UpdateInput(GameTime gameTime)
        {
        }

        public void Update(GameTime gameTime)
        {
            ILevelCommonDataService commonDataService = this._serviceProvider.GetService<ILevelCommonDataService>();

            int i = 0;

            for (i = 0; i < this._texts.Count; ++i)
            {
                float speed = i == this._texts.Count - 1 ? SLOW_TEXT_FADE_SPEED : NORMAL_TEXT_FADE_SPEED;

                FinaleText text = this._texts[i];
                if (text.Alpha < 1.0f)
                {
                    text.Alpha += ((float)gameTime.ElapsedGameTime.TotalSeconds / speed);
                    text.Alpha = Math.Clamp(text.Alpha, 0.0f, 1.0f);
                    break;
                }
            }

            if (this._overlayIndex != (int)LevelTransitions.FinaleOutroFade && i > this._texts.Count / 4)
            {
                this._overlayIndex = (int)LevelTransitions.FinaleOutroFade;
                commonDataService.TransitionOverlays[this._overlayIndex].Start();
            }

            commonDataService.TransitionOverlays[this._overlayIndex].Update(gameTime);
        }

        public void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch)
        {
            graphicsDevice.Clear(Color.White);

            IGameViewportService viewportService = this._serviceProvider.GetService<IGameViewportService>();
            ILevelCommonDataService commonDataService = this._serviceProvider.GetService<ILevelCommonDataService>();
            commonDataService.TransitionOverlays[this._overlayIndex].Draw(graphicsDevice, spriteBatch);

            float lineSpacing = ((float)commonDataService.Font.LineSpacing * 0.5f) * viewportService.Scale;

            float height = viewportService.Height / (this._texts.Count * commonDataService.Font.LineSpacing);
            float currentOffset = commonDataService.Font.LineSpacing + height;

            spriteBatch.Begin();
            for (int i = 0; i < this._texts.Count; ++i)
            {
                FinaleText text = this._texts[i];
                if (text.Alpha == 0.0f)
                {
                    continue;
                }

                Vector2 textSize = commonDataService.Font.MeasureString(text.Text);

                spriteBatch.DrawString(commonDataService.Font, text.Text, new Vector2(viewportService.HalfWidth, currentOffset) - (textSize * 0.5f), new Color(0.0f, 0.0f, 0.0f, text.Alpha));

                currentOffset += height + textSize.Y + lineSpacing;
            }
            spriteBatch.End();    
        }

        public void Stop()
        {
        }

        public void Dispose()
        {
        }
    }
}
